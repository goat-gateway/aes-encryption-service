FROM node:10
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
ENTRYPOINT ["/usr/local/bin/node", "src/app.js"]

